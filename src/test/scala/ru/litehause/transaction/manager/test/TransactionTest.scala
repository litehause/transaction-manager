package ru.litehause.transaction.manager.test

import org.flywaydb.core.Flyway
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import ru.litehause.transaction.manager.exception
import ru.litehause.transaction.manager.models.User
import ru.litehause.transaction.manager.service.TransactionService
import ru.litehause.transaction.manager.util.TransactionLogger
import xitrum.Action

import scala.slick.jdbc.JdbcBackend
import scala.slick.jdbc.JdbcBackend.Database


class TransactionTest extends FlatSpec with Matchers with MockFactory with BeforeAndAfter{

  lazy val database: JdbcBackend.DatabaseDef = {
    Database.forURL("jdbc:h2:mem:test;MODE=PostgreSQL;db_CLOSE_DELAY=-1;AUTOCOMMIT=OFF")
  }

  val logger = stub[TransactionLogger]
  lazy val transactionService = new TransactionService(logger, database)

  val user1 = User(111, "test", 100)
  val user2 = User(112, "test2", 10)

  before {
    database withTransaction { implicit transaction =>
      User.add(user1.id, user1.name, user1.score)
      User.add(user2.id, user2.name, user2.score)
    }
  }

  after {
    database withTransaction { implicit transaction =>
      User.clear()
    }
  }

  it should "Success test transaction" in {
    val transactionSum = 10
    val result = transactionService.transact(user1.id, user2.id, transactionSum)
    assert(result.userFrom.score == user1.score - transactionSum)
    result.userTo.score should equal(user2.score + transactionSum)
  }


  it should "Failed test UserNotFoundException" in {
    intercept[exception.NotFoundException] {
      transactionService.transact(9999, user1.id, 10)
    }
  }

  it should "Failed test translation himself" in {
    intercept[exception.TransactionException] {
      transactionService.transact(user1.id, user1.id, 10)
    }
  }

  it should "Failed test not enough money for translation" in {
    intercept[exception.TransactionException] {
      transactionService.transact(user1.id, user2.id, user1.score + 100)
    }
  }
}
