package ru.litehause.transaction.manager.models

import scala.collection.mutable
import scala.slick.jdbc.JdbcBackend

case class User(id: Long, name: String, score: Long)

object User {

  private val users = mutable.Map[Long, User]()

  def add(id: Long, name: String, score: Long): User = {
    val result = User(id, name, score)
    users.put(id, result)
    result
  }

  def clear(): Unit = {
    users.clear()
  }

  def findById(id: Long)(implicit session:JdbcBackend.SessionDef):Option[User] = {
    users.get(id)
  }

  def find(offset: Long, limit: Long)(implicit session: JdbcBackend.SessionDef): List[User] = {
    users.values.toList
  }

  def total()(implicit session: JdbcBackend.SessionDef):Long = {
    users.size
  }

  def transact(fromUser: User, toUser: User, amount: Long)(implicit session: JdbcBackend.SessionDef): Int = {
    val fu = users.get(fromUser.id)
      .getOrElse(throw new Exception("user not found"))
    val tu = users.get(toUser.id)
      .getOrElse(throw new Exception("user not found"))
    if (fu.score >= amount) {
      users.put(fu.id, fu.copy(score = fu.score - amount))
      users.put(tu.id, tu.copy(score = tu.score + amount))
      2
    } else {
      0
    }
  }
}
