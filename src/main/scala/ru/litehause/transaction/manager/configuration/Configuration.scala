package ru.litehause.transaction.manager.configuration

import com.typesafe.config.ConfigFactory

trait Configuration {
  val config = ConfigFactory.load("database.conf")
    .getConfig("db")
}
