package ru.litehause.transaction.manager.configuration


object DBConfiguration extends Configuration {
  lazy val user = config.getString("user")

  lazy val password = config.getString("password")

  lazy val dbName = config.getString("dbName")

  lazy val host = config.getString("host")

  lazy val driverClassName = config.getString("driverClassName")

  lazy val maxPoolSize = config.getInt("maxPoolSize")

  lazy val minimumIdle = config.getInt("minimumIdle")
}
