package ru.litehause.transaction.manager.action.rest

import ru.litehause.transaction.manager.db.DBConnection
import ru.litehause.transaction.manager.service.{TransactionLogging, TransactionService}
import ru.litehause.transaction.manager.util.{TransactionLogger, TransactionLoggerImpl}
import xitrum.{Action, SkipCsrfCheck}
import xitrum.annotation.{Swagger, POST}

import scala.slick.jdbc.JdbcBackend


@POST("/rest/users/transaction")
@Swagger(
  Swagger.Description("run transaction"),
  Swagger.IntQuery("from", "user id from"),
  Swagger.IntQuery("to", "user id to"),
  Swagger.IntQuery("amount", "transfer amount")
)
class TransactionUser extends AbstractAction with SkipCsrfCheck with DBConnection {

  implicit val transactionLogger = TransactionLoggerImpl

  lazy val transactionService = new TransactionService(TransactionLoggerImpl, db) with TransactionLogging {
    val action = implicitly[Action]
  }

  override def performAction(): AnyRef = {
    val fromUserId = param[Long]("from")
    val toUserId = param[Long]("to")
    val amount = param[Long]("amount")

    transactionService.transact(fromUserId, toUserId, amount)
  }

}
