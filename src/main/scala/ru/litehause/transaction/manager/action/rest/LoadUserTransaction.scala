package ru.litehause.transaction.manager.action.rest

import ru.litehause.transaction.manager.db.DBConnection
import ru.litehause.transaction.manager.exception.NotFoundException
import ru.litehause.transaction.manager.models.{User, Transaction}
import xitrum.annotation
import xitrum.annotation.{Swagger, GET}


case class RespondLoadTransaction(total: Long, transactions: List[Transaction])

@GET("/rest/transactions/from/user/:id<[0-9]+>")
@Swagger(
  Swagger.Description("Get all transaction from user"),
  Swagger.IntPath("id", "user id from"),
  Swagger.OptIntQuery("limit", "default 20"),
  Swagger.OptIntQuery("offset", "default 0")
)
class LoadUserTransaction extends AbstractAction with DBConnection {

  private val DEFAULT_LIMIT = 20L
  private val DEFAULT_OFFSET = 0L

  override def performAction() = {
    val userId = param[Long]("id")
    val limit = paramo[Long]("limit").getOrElse(DEFAULT_LIMIT)
    val offset = paramo[Long]("offset").getOrElse(DEFAULT_OFFSET)
    db withSession { implicit session =>
      val user = User.findById(userId)
        .getOrElse(throw new NotFoundException(s"User with id: $userId not found"))
      val transactions = Transaction.findTransactionsFromUser(user.id, limit, offset)
      val total = Transaction.countTransactionsFromUser(user.id)
      RespondLoadTransaction(total, transactions)
    }
  }
}
