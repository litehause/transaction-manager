package ru.litehause.transaction.manager.action

import io.netty.handler.codec.http.HttpResponseStatus
import xitrum.Action
import xitrum.annotation.{Error500, Error404}

@Error404
class NotFoundError extends Action {
  def execute() {
    response.setStatus(HttpResponseStatus.NOT_FOUND)
    respondJson(Map("status" -> HttpResponseStatus.NOT_FOUND.code()))
  }
}

@Error500
class ServerError extends Action {
  def execute() {
    response.setStatus(HttpResponseStatus.INTERNAL_SERVER_ERROR)
    respondJson(Map("status" -> HttpResponseStatus.INTERNAL_SERVER_ERROR.code()))
  }
}

