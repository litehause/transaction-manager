package ru.litehause.transaction.manager.action.rest

import ru.litehause.transaction.manager.db.DBConnection
import ru.litehause.transaction.manager.models.User
import xitrum.annotation.{GET, Swagger}


case class ResultDTO(total: Long, users: List[User])

@GET("/rest/users/all")
@Swagger(
  Swagger.Description("Load userss list"),
  Swagger.OptStringQuery("limit", "default 20"),
  Swagger.OptStringQuery("offset", "default 0")
)
class LoadUsers extends AbstractAction with DBConnection {
  val DEFAULT_LIMIT = 20L
  val DEFAULT_OFFSET = 0L

  override def performAction(): AnyRef = {
    val limit = paramo[Long]("limit").getOrElse(DEFAULT_LIMIT)
    val offset = paramo[Long]("offset").getOrElse(DEFAULT_OFFSET)
    db withSession { implicit session =>
      val total = User.total()
      val users = User.find(offset, limit)
      ResultDTO(total, users)
    }
  }
}
