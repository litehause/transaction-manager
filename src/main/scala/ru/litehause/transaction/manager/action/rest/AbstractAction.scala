package ru.litehause.transaction.manager.action.rest

import ru.litehause.transaction.manager.exception.NotFoundException
import xitrum.annotation.Swagger
import xitrum.{Log, Action}
import io.netty.handler.codec.http.HttpResponseStatus
case class Respond(status: Int, data: Option[AnyRef], error: Option[Any])

@Swagger(
  Swagger.Tags("REST API management transaction")
)
abstract class AbstractAction extends Action with Log {
  override def execute(): Unit = {
    try {
      respondJson(Respond(HttpResponseStatus.OK.code(), Some(performAction()), None))
    } catch {
      case e: NotFoundException =>
        Log.error(e.getMessage, e)
        response.setStatus(HttpResponseStatus.NOT_FOUND)
        respondJson(Respond(HttpResponseStatus.NOT_FOUND.code(), None, Some(e.getMessage)))
      case e: Exception =>
        Log.error(e.getMessage, e)
        response.setStatus(HttpResponseStatus.INTERNAL_SERVER_ERROR)
        respondJson(Respond(HttpResponseStatus.INTERNAL_SERVER_ERROR.code(), None, Some(e.getMessage)))
    }
  }


  def performAction(): AnyRef
}
