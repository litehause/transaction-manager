package ru.litehause.transaction.manager.action.rest

import ru.litehause.transaction.manager.db.DBConnection
import ru.litehause.transaction.manager.exception.NotFoundException
import ru.litehause.transaction.manager.models.User
import xitrum.annotation.{Swagger, GET}
import xitrum.FutureAction

@GET("/rest/users/:id<[0-9]+>")
@Swagger(
  Swagger.Description("find user by id"),
  Swagger.IntPath("id", "user id")
)
class LoadUser extends AbstractAction with DBConnection {
  override def performAction() = {
    val id = param[Long]("id")
    db withSession { implicit session =>
      User.findById(id)
        .getOrElse(throw new NotFoundException(s"User with id: $id not found"))
    }
  }
}
