package ru.litehause.transaction.manager.service

import ru.litehause.transaction.manager.dto.TransactionResultDTO
import ru.litehause.transaction.manager.exception.{NotFoundException, TransactionException}
import ru.litehause.transaction.manager.models.User
import ru.litehause.transaction.manager.util.TransactionLogger
import scala.slick.jdbc.JdbcBackend

class TransactionService(val logger: TransactionLogger, val db: JdbcBackend.DatabaseDef) extends Transaction {

  private def validateInputParametrs(fromUserId: Long, toUserId: Long, amount: Long) = {
    if (fromUserId == toUserId) {
      throw new TransactionException("Prohibited the translation itself")
    }
    if (amount <= 0) {
      throw new TransactionException("amount must greater than 0")
    }
  }

  private def loadUser(fromUserId: Long, toUserId: Long, amount: Long) = db withSession { implicit session =>
    (User.findById(fromUserId), User.findById(toUserId)) match {
      case (Some(userFrom), Some(userTo)) if userFrom.score > amount =>
        (userFrom, userTo)
      case (Some(_), Some(_)) =>
        throw new TransactionException("not enough money to conduct transaction")
      case _ =>
        throw new NotFoundException(s"Error load user from db from userId: ${fromUserId} to userId ${toUserId}")
    }
  }

  def transact(fromUserId: Long, toUserId: Long, amount: Long): TransactionResultDTO = {
    validateInputParametrs(fromUserId, toUserId, amount)
    val (userFrom, userTo) = loadUser(fromUserId, toUserId, amount)

    db withTransaction { implicit session =>
      if (User.transact(userFrom, userTo, amount) != 2) {
        throw new TransactionException("Error execution transaction")
      }
      (User.findById(fromUserId), User.findById(toUserId)) match {
        case (Some(fromUser), Some(toUser)) => TransactionResultDTO(fromUser, toUser)
        case _ => throw new NotFoundException(s"User not found from db from userId: ${fromUserId} to userId ${toUserId}")
      }
    }
  }
}
