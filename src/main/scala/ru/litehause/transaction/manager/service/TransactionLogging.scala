package ru.litehause.transaction.manager.service

import ru.litehause.transaction.manager.dto.TransactionResultDTO
import ru.litehause.transaction.manager.exception.TransactionException
import ru.litehause.transaction.manager.models.Transaction
import ru.litehause.transaction.manager.util.TransactionLogger
import xitrum.Action

import scala.slick.jdbc.JdbcBackend
import scala.util.Try

trait TransactionLogging extends Transaction {
  this: TransactionService =>

  val action: Action

  abstract override def transact(fromUserId: Long, toUserId: Long, amount: Long): TransactionResultDTO = {
    logger.startTransaction(fromUserId, toUserId, amount, action.remoteIp)
    try {
      val result = super.transact(fromUserId, toUserId, amount)
      logger.successTransaction(fromUserId, toUserId, amount, action.remoteIp)
      db withTransaction { implicit session =>
        Transaction.add(fromUserId, toUserId, amount, Transaction.STATUS_SUCCESS)
      }
      result
    } catch {
      case e: Throwable =>
        logger.errorTransaction(fromUserId, toUserId, amount, Some(e.getMessage), action.remoteIp)
        Try(db withTransaction { implicit session =>
          Transaction.add(fromUserId, toUserId, amount, Transaction.STATUS_ERROR, Some(e.getMessage))
        })
        throw e
    }
  }
}
