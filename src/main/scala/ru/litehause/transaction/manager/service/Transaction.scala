package ru.litehause.transaction.manager.service

import ru.litehause.transaction.manager.dto.TransactionResultDTO

trait Transaction {
  def transact(fromUserId: Long, toUserId: Long, amount: Long): TransactionResultDTO
}
