package ru.litehause.transaction.manager.dto

import ru.litehause.transaction.manager.models.User

case class TransactionResultDTO(userFrom: User, userTo: User)
