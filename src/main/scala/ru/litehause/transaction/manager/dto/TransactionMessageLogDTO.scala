package ru.litehause.transaction.manager.dto

case class TransactionMessageLogDTO(fromUserId: Long, toUserId: Long,
                                    amount: Long, requestIP: String,
                                    status: String, message: Option[String] = None)
