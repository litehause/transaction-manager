package ru.litehause.transaction.manager.exception

class NotFoundException extends Exception {

  var defaultMessage: String = null

  def this(message: String) {
    this()
    defaultMessage = message
  }

  override def getMessage() = {
    defaultMessage
  }
}

