package ru.litehause.transaction.manager.exception

class TransactionException(message: String) extends Exception(message)