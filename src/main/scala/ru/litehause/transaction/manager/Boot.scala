package ru.litehause.transaction.manager

import ru.litehause.transaction.manager.db.DB
import xitrum.Server

object Boot {
  def main(args: Array[String]) {
    DB.db.createConnection().close()
    Server.start()
  }
}
