package ru.litehause.transaction.manager.models

import java.sql.Timestamp

import scala.slick.jdbc.GetResult
import scala.slick.jdbc.StaticQuery.interpolation
import scala.slick.jdbc.JdbcBackend

case class Transaction(id: Long, fromUserId: Long, toUserId: Long, amount: Long,
                       status: String, message: Option[String], date: Timestamp)

object Transaction {

  val STATUS_SUCCESS = "SUCCESS"
  val STATUS_ERROR = "ERROR"

  implicit val getResult = GetResult(r => Transaction(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<?, r.<<))

  def add(fromUserId: Long, toUserId: Long, amount: Long,
          status: String, message: Option[String] = None)(implicit session: JdbcBackend.SessionDef) = {
    sqlu"""
      INSERT INTO transactions(fromUserId, toUserId, amount, status, message)
      VALUES ($fromUserId, $toUserId, $amount, $status, $message)
    """.execute(session)
  }


  def countTransactionsFromUser(userId: Long)(implicit session: JdbcBackend.SessionDef) = {
    sql"""
      SELECT count(*)
      FROM transactions
      WHERE fromUserId = $userId
    """.as[Long].first(session)
  }

  def findTransactionsFromUser(userId: Long, limit: Long, offset: Long)(implicit session: JdbcBackend.SessionDef) = {
    sql"""
      SELECT id, fromUserId, toUserId, amount, status, message, date
      FROM transactions
      WHERE fromUserId = $userId
      ORDER BY date
      LIMIT $limit
      OFFSET $offset
    """.as[Transaction].list(session)
  }
}
