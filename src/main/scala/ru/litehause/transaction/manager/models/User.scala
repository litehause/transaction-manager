package ru.litehause.transaction.manager.models

import scala.slick.jdbc.{JdbcBackend, GetResult}
import scala.slick.jdbc.StaticQuery.interpolation

case class User(id: Long, name: String, score: Long)

object User {
  implicit val getResult = GetResult(r => User(r.<<, r.<<, r.<<))

  def findById(id: Long)(implicit session:JdbcBackend.SessionDef) = {
    sql"""
      SELECT id, name, score
      FROM users
      WHERE id = $id
    """.as[User].firstOption(session)
  }

  def find(offset: Long, limit: Long)(implicit session: JdbcBackend.SessionDef) = {
    sql"""
      SELECT id, name, score
      FROM users
      ORDER BY id
      OFFSET $offset
      LIMIT $limit
    """.as[User].list
  }

  def total()(implicit session: JdbcBackend.SessionDef) = {
    sql"""
      SELECT count(*)
      FROM users
    """.as[Long].first(session)
  }

  def transact(fromUser: User, toUser: User, amount: Long)(implicit session: JdbcBackend.SessionDef) = {
    sqlu"""
      WITH scores AS (
        SELECT
          fromUser.id as fromUserId,
	        toUser.id as toUserId,
	        fromUser.score as fromScore,
	        toUser.score as toScore
        FROM users fromUser
          JOIN users toUser ON toUser.id = ${toUser.id} AND fromUser.id = ${fromUser.id}
            AND (fromUser.score) = ${fromUser.score}
            AND (fromUser.score - $amount) > 0
            AND (toUser.score) = ${toUser.score}
      )
      UPDATE users
      SET score = (CASE id
	      WHEN scores.fromUserId THEN fromScore - ${amount}
        WHEN scores.toUserId THEN toScore + ${amount}
	    END)
      FROM scores
      WHERE id in (scores.fromUserId, scores.toUserId)
    """.first(session)
  }

}
