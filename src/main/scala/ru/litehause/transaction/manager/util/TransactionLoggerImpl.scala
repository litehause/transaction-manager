package ru.litehause.transaction.manager.util

import org.slf4j.LoggerFactory
import ru.litehause.transaction.manager.dto.TransactionMessageLogDTO
import xitrum.Action
import xitrum.util.SeriDeseri

object TransactionLoggerImpl extends TransactionLogger {

  private lazy val logger = LoggerFactory.getLogger("transactions-logger")


  def startTransaction(fromUserId: Long, toUserId: Long, amount: Long, remoteIp: String) = {
    logger.info(SeriDeseri.toJson(TransactionMessageLogDTO(fromUserId, toUserId, amount, remoteIp, STATUS_START_TRANSACTION)))
  }

  def successTransaction(fromUserId: Long, toUserId: Long, amount: Long, remoteIp: String) = {
    logger.info(SeriDeseri.toJson(TransactionMessageLogDTO(fromUserId, toUserId, amount, remoteIp, STATUS_SUCCESS)))
  }

  def errorTransaction(fromUserId: Long, toUserId: Long, amount: Long, message: Option[String], remoteIp: String) = {
    logger.error(SeriDeseri.toJson(TransactionMessageLogDTO(fromUserId, toUserId, amount, remoteIp, STATUS_ERROR, message)))
  }

}
