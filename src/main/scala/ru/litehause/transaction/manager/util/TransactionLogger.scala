package ru.litehause.transaction.manager.util

trait TransactionLogger {
  protected val STATUS_START_TRANSACTION = "START_TRANSACTION"
  protected val STATUS_ERROR = "ERROR"
  protected val STATUS_SUCCESS = "SUCCESS"

  def startTransaction(fromUserId: Long, toUserId: Long, amount: Long, remoteIp: String)

  def successTransaction(fromUserId: Long, toUserId: Long, amount: Long, remoteIp: String)

  def errorTransaction(fromUserId: Long, toUserId: Long, amount: Long, message: Option[String], remoteIp: String)
}
