create table transactions (
  id bigint not null default nextval('main_sequence') primary key ,
  fromUserId bigint not null references users(id),
  toUserId bigint not null references users(id),
  amount bigint not null,
  status varchar(255) not null,
  message text,
  date timestamp default now()
);