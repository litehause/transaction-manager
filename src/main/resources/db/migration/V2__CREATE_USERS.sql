CREATE TABLE users(
  id bigint not null default nextval('main_sequence') primary key,
  name varchar(255) not null,
  score bigint not null default 0
);


INSERT INTO users(id, name, score)
VALUES (1, 'ivan', 500);

INSERT INTO users(id, name, score)
VALUES (2, 'kiril', 355);

INSERT INTO users(id, name, score)
VALUES (3, 'ilia', 100);

INSERT INTO users(id, name, score)
VALUES (4, 'luba', 150);

INSERT INTO users(id, name, score)
VALUES (5, 'tania', 700);

INSERT INTO users(id, name, score)
VALUES (6, 'sergey', 0);

INSERT INTO users(id, name, score)
VALUES (7, 'maxim', 100);

SELECT nextval('main_sequence');
SELECT nextval('main_sequence');
SELECT nextval('main_sequence');
SELECT nextval('main_sequence');
SELECT nextval('main_sequence');
SELECT nextval('main_sequence');
SELECT nextval('main_sequence');