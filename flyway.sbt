import sbt._
import com.typesafe.config.ConfigFactory


lazy val flywayDbConf = settingKey[(String, String, String)]("")
flywayDbConf := {
  val cfg = ConfigFactory.parseFile((baseDirectory in Compile).value / "config/database.conf").getConfig("db")
  val url = s"jdbc:postgresql://${cfg.getString("host")}/${cfg.getString("dbName")}"
  (url, cfg.getString("user"), cfg.getString("password"))
}

flywayUrl := flywayDbConf.value._1
flywayUser := flywayDbConf.value._2
flywayPassword := flywayDbConf.value._3
