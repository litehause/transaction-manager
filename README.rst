Менеджер транзакций

для запуска необходим postges
на нем надо создать базу transaction_manager и пользователя pg_transaction_manager и выдать ему права пример скрипта в файле CREATE_DATABASE.sql
если хотите использовать другую бд и/или пользователя нужно откоректировать файл /config/database.conf

для запуска прилоения запустите ./runner.sh
или
::

  sbt clean test flywayMigrate run

flywayMigrate необходим для выполнения миграций

после чего приложение заработает на порту http://localhost:8000/xitrum/swagger или https://localhost:4430/xitrum/swagger
если надо подкорекитровать порт то надо зайти в файл /config/xitrum.conf

для того чтоб собрать готовое приложение выполните ./compiler.sh и следуйте указаниям
